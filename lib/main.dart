import 'package:flutter/material.dart';
import 'package:tann_mann/screens/about_screen.dart';
import 'package:tann_mann/screens/contact_us_screen.dart';
import 'package:tann_mann/screens/home_screen.dart';
import 'screens/welcome_screen.dart';
import 'screens/login_screen.dart';
import 'screens/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
      initialRoute: WelcomeScreen.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id:(context) => LoginScreen(),
        ContactUsScreen.id:(context) =>ContactUsScreen(),
        AboutScreen.id:(context) =>AboutScreen(),
        HomeScreen.id:(context) =>HomeScreen()
      },
    );
  }
}