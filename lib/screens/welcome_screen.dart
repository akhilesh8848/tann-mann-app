import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tann_mann/components/rounded_button.dart';
import 'login_screen.dart';

class WelcomeScreen extends StatefulWidget {
  static String id = 'welcome_screen';
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {

  void _showcontent(){
    showDialog(context: context,barrierDismissible: false,builder: (BuildContext context){
      return new AlertDialog(title: Text('You clicked on Register'),
        content:SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("Please use the Login Button")
            ],
          ),
        ) ,
        actions: <Widget>[
          FlatButton(
            child: Text("Ok",style: TextStyle(color: Color.fromARGB(255, 224, 144, 47),),),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 14.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              color: Color.fromARGB(255, 224, 144, 47),
              child: Row(
                children: <Widget>[
                  Hero(
                    tag: 'logo',
                    child: Container(
                      child:Image.asset('assets/image.jpeg'),
                      height: 60.0,
                    ),
                  ),
                  Text('The Tann Mann Gaadi',
                    style: TextStyle(
                      shadows: [
                        Shadow(
                          color: Colors.white,
                          offset: Offset(0,-10)
                        )
                      ],
                      fontSize: 27.0,
                      color:Colors.transparent,
                      decoration: TextDecoration.underline,
                      decorationColor: Colors.black,
                      decorationThickness: 2
                      //fontWeight: FontWeight.w900,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            RoundedButton(
              text: 'Log in',
              onPressed: () {
                Navigator.pushNamed(context, LoginScreen.id);
              },
              color: Color.fromARGB(255, 224, 144, 47),
            ),
            RoundedButton(
              text: 'Register',
              onPressed: () {
                _showcontent();
              },
              color: Color.fromARGB(255, 224, 144, 47),
            ),
            SizedBox(height: 10,),
            Divider(height:30,thickness:3,color: Color.fromARGB(255, 224, 144, 47),),
            Text('or Log in using',textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold,color: Color.fromARGB(255, 224, 144, 47),),),
            Image.asset('assets/image4.png')
          ],
        ),
      ),
    );
  }
}