import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AboutScreen extends StatelessWidget {
  static String id = 'about_screen';
  static String bullet='\u2022';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 224, 144, 47),
        title: Text('About Us'),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
            child: Column(
              children: <Widget>[
                Image.asset('assets/image2.jpg'),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.keyboard_arrow_up),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      'About The Tann Mann Gaadi',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 224, 144, 47)),
                    ),
                    SizedBox(
                      width: 11,
                    ),
                    Container(
                      color: Color.fromARGB(255, 224, 144, 47),
                      child: Text('                                   '),
                      width: 65,
                      height: 22,
                    )
                  ],
                ),
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,

                        ),
                        children: <TextSpan>[
                          TextSpan(text: 'We are a '),
                          TextSpan(
                              text: " 'Not for Profit' ",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: ' trust venture building Wellness on wheels '),
                          TextSpan(
                              text: " (The Tann Mann Gadi) ",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: ' to address the serious problem of open defecation which is a major health risk for all in developing countries. \n\n'),
                          TextSpan(text:"We are in the mission of integrating technology with our social cause.\n\n" ),
                          TextSpan(text: 'We are in the process of providing a holistic approach to social responsibility keeping health, sensitization, safety and Eco-friendly solutions\n'),
                        ]),
                  ),
                ),
                Text("\nWhy Tann Mann Gaadi\n",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 20,
                    color: Color.fromARGB(255, 224, 144, 47),
                    fontWeight: FontWeight.bold
                  ),
                ),
                Text('$bullet  Open defecation which is a major health risk globally.\n',textAlign: TextAlign.left,),
                Text('$bullet  Inaccessibility of Hygienic & Safe public conveniences.\n',textAlign: TextAlign.left,),
                Text('$bullet  Un-safe environment for women, children, differently abled & the elderly.\n',textAlign: TextAlign.center,),
                Text('$bullet  Lack of potable and  clean water.\n',textAlign: TextAlign.left,),
                Text('$bullet  Un-availability of basic health facilities & amenities.\n',textAlign: TextAlign.left,),
              ],
            )
        ),
      ),
    );
  }
}
