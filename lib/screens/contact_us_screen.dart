import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tann_mann/components/rounded_button.dart';

class ContactUsScreen extends StatefulWidget {
  static String id='contact_us_screen';
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  String name,email,message;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 224, 144, 47),
        title: Text('Contact Us'),
      ),
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 5.0),
          child: Column(
            children: <Widget>[
//              Text(
//                'Drop us a message',
//                textAlign: TextAlign.right,
//                style: TextStyle(
//                  fontSize: 20,
//                  fontWeight: FontWeight.bold
//                ),
//              ),
//              SizedBox(height: 20),
              TextField(
                  textAlign: TextAlign.left,
                  onChanged: (value) {
                    name = value;
                  },
                  decoration:
                  InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    hintText: 'Name',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color:Color.fromARGB(255, 224, 144, 47),width: 2.0),
                    ),

                  )
              ),
              SizedBox(height: 20),
              TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.left,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration:
                  InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    hintText: 'E-mail',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color:Color.fromARGB(255, 224, 144, 47),width: 2.0)
                    )
                  )
              ),
              SizedBox(height: 20),
              TextField(
                  maxLines: 10,
                  textAlign: TextAlign.left,
                  onChanged: (value) {
                    message = value;
                  },
                  decoration:
                  InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    hintText: 'Add your Message',
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color:Color.fromARGB(255, 224, 144, 47),width: 2.0),
                  )
              ),
              ),
              RoundedButton( text: 'Submit',
                onPressed: ()  {
                  //Navigator.pushNamed(context, ContactUsScreen.id);
                },
                color: Color.fromARGB(255, 224, 144, 47),)
            ],
          ),
        ),
      );
  }
}
