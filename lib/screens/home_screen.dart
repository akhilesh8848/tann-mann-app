import 'package:flutter/material.dart';
import 'package:tann_mann/components/rounded_button.dart';
import 'package:tann_mann/screens/about_screen.dart';
import 'package:tann_mann/screens/contact_us_screen.dart';

class HomeScreen extends StatelessWidget {
  static String id='home_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 224, 144, 47),
        title: Text('The Tann Mann Gaadi App'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0,vertical: 8.0),
        child: Column(
          children: <Widget>[
            Text('Thank you for joining us on this initiative.\n',style: TextStyle(
              fontSize: 17,
              color: Color.fromARGB(255, 224, 144, 47),
            ),),
            SizedBox(height: 4),
            Image.asset('assets/image.jpeg'),
            SizedBox(height: 4,),
            RoundedButton(
              text: 'Contact Us',
              color: Color.fromARGB(255, 224, 144, 47),
              onPressed: () {
                Navigator.pushNamed(context, ContactUsScreen.id);
              },
            ),
            SizedBox(width: 1.4,),
            RoundedButton(
              text: 'About TTMG',
              color: Color.fromARGB(255, 224, 144, 47),
              onPressed: () {
                Navigator.pushNamed(context, AboutScreen.id);
              },
            ),
            SizedBox(height: 25,),
            Image.asset('assets/image3.jpg'),
          ],
        ),
      ),
    );
  }
}
